import 'dart:async';

import 'package:flutter/material.dart';

class CustomSlider extends StatefulWidget {
  CustomSlider({Key key, this.height, this.items, this.onChanged,}) : super(key: key);

  final double height;
  final List<Widget> items;
  final Function(int) onChanged;

  @override
  _CustomSliderState createState() => _CustomSliderState();
}

class _CustomSliderState extends State<CustomSlider> with TickerProviderStateMixin {
  PageController _pageController;
  final int realPage = 10000;
  int initialPage = 0;
  @override
  void initState() { 
    super.initState();
    _pageController = PageController(
      viewportFraction: 0.8,
      initialPage: realPage + initialPage,
    )..addListener(pageChange);
  }

  void pageChange() {
    
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      child: PageView.builder(
        physics: PageScrollPhysics(),
        scrollDirection: Axis.horizontal,
        controller: _pageController,
        reverse: false,
        itemCount: null,
        onPageChanged: (i) {
          final int index = _getRealIndex(
            i + initialPage, 
            realPage,
            widget.items.length,
          );
          widget.onChanged(index);
        },
        itemBuilder: (context, i) {
          final int index = _getRealIndex(
            i + initialPage, 
            realPage,
            widget.items.length,
          );

          return AnimatedBuilder(
            animation: _pageController,
            child: widget.items[index],
            builder: (BuildContext context, Widget child) {
              if (_pageController.position.minScrollExtent == null ||
                  _pageController.position.maxScrollExtent == null) {
                    Future.delayed(Duration(microseconds: 1), () {
                      if (this.mounted) {
                        setState(() {});
                      }
                    });
                    return Container();
                  }
              
              double value = _pageController.page - 1;
              value = (1 - (value.abs() * 0.3)).clamp(0.0,1.0);

              return Center(
                child: SizedBox(
                  height: widget.height,
                  child: child,
                ),
              );
            },
          );
        },
      ),
    );
  }
}

/// Converts an index of a set size to the corresponding index of a collection of another size
/// as if they were circular.
///
/// Takes a [position] from collection Foo, a [base] from where Foo's index originated
/// and the [length] of a second collection Baa, for which the correlating index is sought.
///
/// For example; We have a Carousel of 10000(simulating infinity) but only 6 images.
/// We need to repeat the images to give the illusion of a never ending stream.
/// By calling _getRealIndex with position and base we get an offset.
/// This offset modulo our length, 6, will return a number between 0 and 5, which represent the image
/// to be placed in the given position.
int _getRealIndex(int position, int base, int length) {
  final int offset = position - base;
  return _remainder(offset, length);
}

/// Returns the remainder of the modulo operation [input] % [source], and adjust it for
/// negative values.
int _remainder(int input, int source) {
  if (source == 0) return 0;
  final int result = input % source;
  return result < 0 ? source + result : result;
}

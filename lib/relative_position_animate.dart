import 'package:flutter/material.dart';

class RelativePositionedTransitionPage extends StatefulWidget {
  static const routeName = 'RelativePositionedTransition';

  @override
  _RelativePositionedTransitionPageState createState() => _RelativePositionedTransitionPageState();
}

class _RelativePositionedTransitionPageState extends State<RelativePositionedTransitionPage>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  var _isMoved = false;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Stack(
        children: [
          RelativePositionedTransition(
            rect: _animationController
                .drive(
                  CurveTween(
                    curve: Curves.fastOutSlowIn,
                  ),
                )
                .drive(
                  RectTween(
                    begin: Rect.fromLTRB(0, 0, 0, 0),
                    end: Rect.fromLTRB(0, 0, 10, 0),
                  ),
                ),
            size: const Size(50, 50),
            child: Material(
              color: Colors.amberAccent,
              child: InkWell(
                onTap: () {
                  if (_isMoved) {
                    _animationController.reverse();
                  } else {
                    _animationController.forward();
                  }
                  _isMoved = !_isMoved;
                },
                child: SizedBox(height: 30),
              ),
            ),
          )
        ],
      ),
    );
  }
}

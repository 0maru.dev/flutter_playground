import 'package:flutter/material.dart';

class SizeTransitionPage extends StatefulWidget {
  static const routeName = 'SizeTransition';

  @override
  _SizeTransitionPageState createState() => _SizeTransitionPageState();
}

class _SizeTransitionPageState extends State<SizeTransitionPage>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  var _isScaledUp = false;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          if (_isScaledUp) {
            _animationController.reverse();
          } else {
            _animationController.forward();
          }
          _isScaledUp = !_isScaledUp;
        },
        child: const Icon(Icons.refresh),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                const Spacer(),
                SizeTransition(
                  axis: Axis.horizontal,
                  axisAlignment: 0.1,
                  sizeFactor: _animationController
                      .drive(
                        CurveTween(curve: Curves.fastOutSlowIn),
                      )
                      .drive(
                        Tween<double>(
                          begin: 1,
                          end: 0.95,
                        ),
                      ),
                  child: Padding(
                    padding: const EdgeInsets.all(4),
                    child: Material(
                      color: Colors.amberAccent,
                      child: InkWell(
                        onTap: () async {
                          _animationController.forward();
                          await Future.delayed(const Duration(milliseconds: 300));
                          _animationController.reverse();
                        },
                        child: SizedBox(
                          height: 30,
                          // width: double.infinity - 32,
                          width: 300,
                          child: Text('test'),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

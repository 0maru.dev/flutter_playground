import 'package:flutter/material.dart';
import 'package:flutter_playground/calender_carousel.dart';

class NewCalendarPage extends StatefulWidget {
  NewCalendarPage({Key key}) : super(key: key);

  @override
  _NewCalendarPageState createState() => _NewCalendarPageState();
}

class _NewCalendarPageState extends State<NewCalendarPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black26,
      appBar: AppBar(),
      body: SafeArea(
        child: CalenderCarousel(
          selectedDate: DateTime.now(),
          height: MediaQuery.of(context).size.height,
        ),
      ),
    );
  }
}

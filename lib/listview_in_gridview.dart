import 'package:flutter/material.dart';

class ListViewInGridViewPage extends StatelessWidget {
  const ListViewInGridViewPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cellWidth = (MediaQuery.of(context).size.width - 32) / 3;
    final cellHeight = cellWidth * 1.7;

    return Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        child: ListView.builder(
          itemCount: 10,
          itemBuilder: (BuildContext context, int index) {
            return Card(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: index <= 3 ? cellHeight : cellHeight * (index % 3),
                  decoration: BoxDecoration(color: index % 2 == 0 ? Colors.red : Colors.blue),
                  child: GridView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: index,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 8,
                      mainAxisSpacing: 8,
                      childAspectRatio: 1 / 1.7,
                    ),
                    itemBuilder: (_, index) {
                      return Card(
                        color: Colors.white,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration:
                                BoxDecoration(color: index % 2 == 1 ? Colors.red : Colors.blue),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

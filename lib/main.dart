import 'package:flutter/material.dart';
import 'package:flutter_playground/calender.dart';
import 'package:flutter_playground/carousel_slider.dart';
import 'package:flutter_playground/endless_pageview.dart';
import 'package:flutter_playground/listview_in_gridview.dart';
import 'package:flutter_playground/new_calender.dart';
import 'package:flutter_playground/relative_position_animate.dart';
import 'package:flutter_playground/size_transition_animation.dart';

import 'ScaleListTile.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            title: Text('RelativePositionedTransitionPage(),'),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => RelativePositionedTransitionPage(),
                ),
              );
            },
          ),
          ListTile(
            title: Text('SizeTransitionPage'),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (_) => SizeTransitionPage(),
                ),
              );
            },
          ),
          ListTile(
            title: Text('EndlessPageView'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (_) => Carroussel()));
            },
          ),
          ListTile(
            title: Text('carousel'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (_) => CarouselSlide()));
            },
          ),
          ListTile(
            title: Text('calender'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (_) => CalendarPage()));
            },
          ),
          ListTile(
            title: Text('new calender'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (_) => NewCalendarPage()));
            },
          ),
          ListTile(
            title: Text('ListViewInGridViewPage'),
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => ListViewInGridViewPage()));
            },
          )
        ],
      ),
    );
  }
}

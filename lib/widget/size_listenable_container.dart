import 'package:flutter/widgets.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';

class FlexibleContainer extends SingleChildRenderObjectWidget {
  const FlexibleContainer({
    Key key,
    Widget child,
    @required this.onChanged,
  })  : assert(onChanged != null),
        super(key: key, child: child);

  final void Function(Size) onChanged;

  @override
  _FlexibleContainerRenderObject createRenderObject(context) =>
      _FlexibleContainerRenderObject(onChanged: onChanged);
}

class _FlexibleContainerRenderObject extends RenderProxyBox {
  _FlexibleContainerRenderObject({
    RenderBox child,
    @required this.onChanged,
  })  : assert(onChanged != null),
        super(child);

  final void Function(Size) onChanged;
  Size _oldSize;

  @override
  void performLayout() {
    super.performLayout();

    final size = this.size;
    if (size != _oldSize) {
      _oldSize = size;
      _callback(size);
    }
  }

  void _callback(Size size) {
    SchedulerBinding.instance.addPostFrameCallback((callback) => onChanged(size));
  }
}

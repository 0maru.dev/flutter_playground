import 'package:flutter/material.dart';

class ScaleListTile extends StatefulWidget {
  const ScaleListTile({
    Key key,
    this.index,
  }) : super(key: key);

  final int index;
  @override
  _ScaleListTileState createState() => _ScaleListTileState();
}

class _ScaleListTileState extends State<ScaleListTile> with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  var _isMoved = false;
  final curveTween = CurveTween(
    curve: const Interval(0.3, 1, curve: Curves.fastOutSlowIn),
  );
  final tween = Tween<double>(begin: 1, end: 0.7);

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 3000),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RelativePositionedTransition(
      size: const Size(50, 50),
      rect: _animationController
          .drive(
            CurveTween(
              curve: Curves.elasticInOut,
            ),
          )
          .drive(
            RectTween(
              begin: Rect.fromLTRB(10, 10, -100, -400),
              end: Rect.fromLTRB(200, 500, 0, 0),
            ),
          ),
      child: Image.network(
        'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.amazon.co.jp%2F%25E3%2582%25A4%25E3%2583%259E%25E3%2583%25BC%25E3%2582%25B8%25E3%2583%25A5-%25E3%2582%25AA%25E3%2583%25A0%25E3%2583%258B%25E3%2583%2590%25E3%2582%25B9%2Fdp%2FB000051TAX&psig=AOvVaw2LC-XFwOOIQW0RPCNiuDXX&ust=1584262592293000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCLDazJrMmegCFQAAAAAdAAAAABAD',
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _buildChiuld(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 32, top: 8, right: 8),
      child: Center(
        child: Material(
          color: Colors.amberAccent,
          child: InkWell(
            child: SizedBox(
              child: Center(
                child: Text('index ${widget.index}'),
              ),
              height: 40,
            ),
            onTap: () {
              if (_isMoved) {
                _animationController.reverse();
              } else {
                _animationController.forward();
              }
              _isMoved = !_isMoved;
            },
          ),
        ),
      ),
    );
  }
}

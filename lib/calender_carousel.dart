import 'package:flutter/material.dart';

class CalenderCarousel extends StatefulWidget {
  CalenderCarousel({
    Key key,
    this.selectedDate,
    this.child,
    this.height,
    this.width,
  }) : super(key: key);

  final DateTime selectedDate;
  final double height;
  final double width;
  final Widget child;

  @override
  _CalenderCarouselState createState() => _CalenderCarouselState();
}

class _CalenderCarouselState extends State<CalenderCarousel> {
  PageController _controller;
  DateTime _selectedDate = DateTime.now();
  int _pageNum = 0;
  DateTime minDate;
  DateTime maxDate;
  DateTime _targetDate;
  List<DateTime> _dates;

  @override
  void initState() {
    super.initState();
    final now = DateTime.now();
    // 過去4年未来4年分
    minDate = DateTime(now.year - 4);
    maxDate = DateTime(now.year + 4);

    // 選択する日を設定　指定がなければ今日
    if (widget?.selectedDate != null) {
      _selectedDate = widget.selectedDate;
    }

    _targetDate = _selectedDate;
    for (int i = 0;
        0 >
            DateTime(minDate.year, minDate.month + i)
                .difference(DateTime(_targetDate.year, _targetDate.month))
                .inDays;
        i += 1) {
      _pageNum = i + 1;
    }

    _controller = PageController(initialPage: _pageNum);

    List<DateTime> date = [];
    for (int _cnt = 0;
        0 >=
            DateTime(minDate.year, minDate.month + _cnt)
                .difference(DateTime(maxDate.year, maxDate.month))
                .inDays;
        _cnt++) {
      date.add(DateTime(minDate.year, minDate.month + _cnt, 1));
      if (0 == date.last.difference(DateTime(_targetDate.year, _targetDate.month)).inDays) {}
    }
    _dates = date;
  }

  void setDate([int page = -1]) {
    if (page == -1) {
      // setState(() => setDatesAndWeeks());
    } else {
      setState(() {
        _pageNum = page;
        _targetDate = _dates[page];
      });
      _controller.animateToPage(
        page,
        duration: const Duration(milliseconds: 1),
        curve: const Threshold(0),
      );
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: PageView.builder(
        controller: _controller,
        itemCount: _dates.length,
        physics: const ScrollPhysics(),
        scrollDirection: Axis.horizontal,
        onPageChanged: setDate,
        pageSnapping: true,
        itemBuilder: (_, index) {
          return _buildAnimateBuilder(context, index);
        },
      ),
    );
  }

  Widget _buildAnimateBuilder(BuildContext context, int index) {
    final year = _dates[index].year;
    final month = _dates[index].month;

    return AnimatedBuilder(
      animation: _controller,
      child: SizedBox(
        height: 399,
        width: 400,
        child: Card(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(32),
            child: Container(
              decoration: BoxDecoration(color: index % 2 == 0 ? Colors.red : Colors.blue),
              child: Text('${year}年 ${month}月'),
            ),
          ),
        ),
      ),
      builder: (BuildContext context, Widget child) {
        double value = 1.0;
        if (_controller.position.haveDimensions) {
          value = _controller.page - index;
          value = (1 - (value.abs() * 5)).clamp(0.0, 1.0);
        }

        return Center(child: child);
      },
    );
  }
}

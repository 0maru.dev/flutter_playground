import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_playground/slider.dart';

class CarouselSlide extends StatefulWidget {
  const CarouselSlide({Key key}) : super(key: key);

  @override
  _CarouselSlideState createState() => _CarouselSlideState();
}

class _CarouselSlideState extends State<CarouselSlide> {

  /// カレンダーで選ばれている年月
  DateTime selectedDate;

  /// 0 ~ 11 のインデックス
  int currentIndex = 0;

  /// アプリ起動時の月
  int currentMonth;

  @override
  void initState() { 
    super.initState();
    selectedDate = DateTime.now();
    currentMonth = selectedDate.month;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${selectedDate.year}年　${selectedDate.month}月'),
      ),
      body: Center(
        child: CustomSlider(
          onChanged: changeMonth,
          height: 400.0,
          items: [1,2,3,4,5,6,7,8,9,10,11,12].map((i) {
          // items: [1,2, 3].map((i) {
            return Builder(
              builder: (BuildContext context) {
                return Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.symmetric(horizontal: 5.0),
                  decoration: BoxDecoration(
                    color: Colors.amber
                  ),
                  child: Text('text $i', style: TextStyle(fontSize: 16.0),)
                );
              },
            );
          }).toList(),
        ),
      ),
    );
  }

  void changeMonth(int i) {
    // (currentMonth + currentIndex) % 12 == 0 ? 12 : (currentMonth + currentIndex) % 12
    final prevMonth = (currentMonth + currentIndex) % 12 == 0 ? 12 : (currentMonth + currentIndex) % 12;
    setState(() {
      currentIndex = i;
    });
    final nextMonth = (currentMonth + currentIndex) % 12 == 0 ? 12 : (currentMonth + currentIndex) % 12;
    setMonth(nextMonth);
    if ((prevMonth - nextMonth) == -11) {
      /// 一年前に行く
      print('前');
      prevYear();
    }
    if ((prevMonth - nextMonth) == 11) {
      /// 一年進む
      print('後ろ');
      nextYear();
    }
  }

  void setMonth(int month) {
    setState(() {
      selectedDate = DateTime(
        selectedDate.year,
        month,
      );
    });
  }

  void prevYear() {
    final currentDate = selectedDate;
    setState(() {
      selectedDate = DateTime(
        currentDate.year - 1,
        12,
      );
    });
  }

  void nextYear() {
    final currentDate = selectedDate;
    setState(() {
      selectedDate = DateTime(
        currentDate.year + 1,
        1,
      );
    });
  }
}